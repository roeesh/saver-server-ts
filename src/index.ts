import log from "@ajar/marker";
import net from "net";
import  fs  from "fs/promises";

const { port } = process.env;

const server = net.createServer();

server.on("connection", socket => {
  log.yellow("✨ Backup connected ✨");
  socket.write("send files to save.");

  socket.on("data", async (buffer) => {
   
    // log.v("data:",buffer.toString());
       
    const {data,fileName} = JSON.parse(buffer.toString()); 
      await fs.appendFile(`${fileName}.sql`, data);
      socket.write(Buffer.from(`{"saved file":${fileName}}\n`));
    });
     
    socket.on("end", () => { 
      log.yellow("✨ backup disconnected ✨");
    });
});

server.on("error", err => log.error(err));
server.listen(port, () => log.v("✨ Saver is up  🚀"));